const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: 8081,
        allowedHosts: [
            'img.local',
        ],
    },
}