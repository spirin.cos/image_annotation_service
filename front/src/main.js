import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import {BootstrapVue, IconsPlugin} from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import axios from "axios";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

router.beforeEach((to, from, next) => {

    if (!router.options.meta.user.uid) {
        router.options.update_authorized_user_data();
    }
    console.log('before each', router.options.meta);
    next()
});

new Vue({
    data: {
        meta: router.options.meta,

    },
    methods: {
        make_notification: function (message, title = 'error', variant = 'danger') {
            // документация https://bootstrap-vue.org/docs/components/toast
            this.$bvToast.toast(message, {
                title: title,
                autoHideDelay: 10000,
                variant: variant,
            })
        },
        login: function (uid, password) {
            router.options.reset_meta();
            return axios({
                url: '/auth/login/',
                method: 'POST',
                data: {
                    uid: uid,
                    password: password,
                },
            })

        },

    },
    router,
    render: h => h(App)
}).$mount("#app");
