import Vue from "vue";
import VueRouter from "vue-router";
import axios from "axios";

Vue.use(VueRouter);

const routes = [


];


const router = new VueRouter({
    mode: 'history',
    routes: routes,

    meta: {
        user: {
            uid: undefined,
            permissions: {},
        }
    },

    reset_meta: function () {
        console.log('reset user data')
        this.meta.user.uid = undefined;
    },

    update_user_data: function (auth_data) {
        this.meta.user.uid = auth_data.user.uid;
    },

    update_authorized_user_data: function () {
        axios({
            url: '/api/user/authorization/',
            method: 'GET',

        }).then(response => {
            this.update_user_data(response.data)
        }).catch(error => {
            console.log('error auth ', error)
        })
    },
    logout: function () {
        this.reset_meta()

        axios({
            url: '/auth/logout/',
            method: 'POST',
            params: {},
        })

    },
});

export default router;
