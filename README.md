Image annotation service
====
Сервис разметки изображений через телеграм бот. Выполнено в качестве тестового задания за ~12ч, условия задачи можно найти в файле task.pdf.

Частью задания было задеплоиться, поэтому можно посмотреть что получилось в действии:

* cсылка на web интерфейс: http://onamesan.space (логин и пароль: admin admin)
* cсылка на бота: https://t.me/TestImageAnnotationBot




Требования проекта
----
* python >= 3.8
* библиотеки для python перечислены в файле requirements.txt
* postgres (PostgreSQL) 11.5
    * ENCODING 'UTF-8'
    * LC_COLLATE 'ru_RU.UTF-8'
    * LC_CTYPE 'ru_RU.UTF-8'
  
Запуск
-----
Для запуска необходимо заполнить configs/config.yaml, configs/logger.json.

    python3 -m venv env  \\ создайте новое вириальное окружение
    source env/bin/activate  \\ активируйте созданное окружение
    pip3 install -r requirements.txt  \\ установите зависимости
    aerich upgrade \\ запустить миграции
    python -m image_annotation_service.main_back \\ запуск бекэнда
    python -m image_annotation_service.main_bot \\ запуск бота
