from typing import Union, List

from tortoise import Model, fields

from image_annotation_service.models.array_field import IntArrayField


class UserRoles:
    user: str = 'user'
    admin: str = 'admin'





class Classification(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(40, null=False, unique=True)

    created = fields.DatetimeField(auto_now_add=True, null=False)
    updated = fields.DatetimeField(auto_now=True, null=False)


class Object(Model):
    id = fields.IntField(pk=True)
    image_path = fields.TextField(null=False)

    classification_set = IntArrayField()

    created = fields.DatetimeField(auto_now_add=True, null=False)
    updated = fields.DatetimeField(auto_now=True, null=False)

    classifications: fields.ReverseRelation["ObjectClassification"]


class User(Model):
    id = fields.IntField(pk=True)

    uid = fields.CharField(64, null=False, unique=True)
    password = fields.CharField(40, null=False)

    role = fields.CharField(16, default=UserRoles.user, null=False)
    telegram_id = fields.IntField(null=True)

    created = fields.DatetimeField(auto_now_add=True, null=False)
    updated = fields.DatetimeField(auto_now=True, null=False)

    async def get_unmarked_object(self) -> Union[Object, None]:
        unmarked_object = await Object.filter(
            id__not_in=await ObjectClassification.filter(user_id=self.id).values_list('object_id', flat=True)).first()
        return unmarked_object

    async def get_unmarked_objects(self) -> List[Object]:
        unmarked_objects = await Object.filter(
            id__not_in=await ObjectClassification.filter(user_id=self.id).values_list('object_id', flat=True)).all()
        return unmarked_objects


class ObjectClassification(Model):
    id = fields.IntField(pk=True)

    object: fields.ForeignKeyRelation[Object] = fields.ForeignKeyField(
        'models.Object',
        related_name='objects',
    )

    classification: fields.ForeignKeyRelation[Classification] = fields.ForeignKeyField(
        'models.Classification',
        related_name='classifications',
    )

    user: fields.ForeignKeyRelation[User] = fields.ForeignKeyField(
        'models.User',
        related_name='relation_with_user',
    )

    created = fields.DatetimeField(auto_now_add=True, null=False)
    updated = fields.DatetimeField(auto_now=True, null=False)

