

from aiohttp import web
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from tortoise.contrib.aiohttp import register_tortoise

from image_annotation_service.lib.configs import config, TORTOISE_ORM_CONFIG
from image_annotation_service.lib.security import access_check
from image_annotation_service.routes import setup_routes
from aiohttp_session import setup as session_setup


def init_app():
    app = web.Application()
    app['config'] = config

    # setup views and routes
    setup_routes(app)

    # setup session
    session_setup(app, EncryptedCookieStorage(
        config['secret_key'],
        cookie_name=config['cookie_name'])
    )

    app.middlewares.append(access_check)

    register_tortoise(
        app, config=TORTOISE_ORM_CONFIG
    )

    return app


def main():
    app = init_app()
    web.run_app(app,
                host=config['host'],
                port=config['port'])


if __name__ == '__main__':
    main()
