
from aiohttp.web import middleware, Request, Response
from aiohttp.web_response import json_response
from aiohttp_session import get_session

from image_annotation_service.lib.handles.authorization import clear_session
from image_annotation_service.models.models import UserRoles


@middleware
async def access_check(request: Request, handler) -> Response:
    session = await get_session(request)

    if not request.path.startswith(('/auth/', '/health/')) and request.match_info.get_info().get('http_exception') is None:

        if 'uid' not in session:
            return json_response(status=401, data={'status_code': 401, 'reason': f'Not authorized'})
        if 'role' not in session:
            await clear_session(session)
            return json_response(status=401, data={'status_code': 401, 'reason': f'Not authorized'})

        if session['role'] != UserRoles.admin:
            await clear_session(session)
            return json_response(status=403, data={'status_code': 403, 'reason': f'Permission denied'})

    if request.match_info.get_info().get('http_exception'):
        data = {}

        status_code = request.match_info.get_info().get('http_exception').status_code
        data['status_code'] = status_code
        if request.match_info.get_info().get('http_exception').reason:
            data['reason'] = request.match_info.get_info().get('http_exception').reason

        response = json_response(status=status_code, data=data)
        return response

    response: Response = await handler(request)
    return response
