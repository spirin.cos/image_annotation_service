from aiohttp.web import Response, Request, json_response
from aiohttp_session import get_session

from image_annotation_service.models import User
from image_annotation_service.models.models import UserRoles


async def clear_session(session):
    if 'uid' in session:
        del session['uid']
    if 'role' in session:
        del session['role']


async def get_authorization_data(request: Request) -> Response:
    session = await get_session(request)
    uid = session.get('uid')
    if not uid:
        return json_response(status=200, data={'status_code': 401, 'reason': f'Нou are not authorized'})

    return json_response(status=200, data=await get_authorization_user_data(uid))


async def get_authorization_user_data(uid: str) -> dict:
    return {'user': {'uid': uid}}


async def login(request: Request) -> Response:
    session = await get_session(request)
    login_data = await request.json()
    uid = login_data.get('uid')
    password = login_data.get('password')

    if not uid or not password:
        return json_response(status=400, data={'status_code': 400, 'reason': f'Incorrect request'})

    user = await User.filter(uid=uid, password=password, role=UserRoles.admin).first()

    if not user:
        return json_response(status=401, data={'status_code': 403, 'reason': f'Incorrect login or password'})

    session['uid'] = uid
    session['role'] = user.role
    return json_response(status=200, data=await get_authorization_user_data(uid))


async def logout(request: Request) -> Response:
    # Removing user data from the current session
    session = await get_session(request)
    await clear_session(session)
    return json_response(status=200, data={})
