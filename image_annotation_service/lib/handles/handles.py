import json

from aiohttp.web import Response, Request
from aiohttp.web_response import json_response
from aiohttp_session import get_session
import uuid
import os

from image_annotation_service.models import Object, Classification


async def index(request: Request) -> Response:
    return json_response(status=200, data={})


async def create_classification(request: Request):
    name: str = request.query.get('name')
    if not name:
        return json_response(status=400, data={'status_code': 400, 'reason': f'Incorrect request'})

    classification: Classification = await Classification.filter(name=name).first()

    if classification:
        return json_response(status=200, data={'name': classification.name, 'id': classification.id})

    classification = await Classification.create(name=name)

    return json_response(status=201, data={'name': classification.name, 'id': classification.id})


async def get_classification(request: Request) -> Response:
    objects = await Classification.all().values(
        id="id",
        name='name',
    )
    return Response(text=json.dumps(objects, default=str), content_type='application/json')


async def create_object(request: Request):
    save_image_path = request.app['config']['save_image_path']
    post = await request.post()
    image = post.get("image")
    if not image:
        return json_response(status=400, data={'status_code': 400, 'reason': f'Incorrect request'})

    img_content = image.file.read()
    file_name = f'{uuid.uuid1()}_{image.filename}'

    full_image_path = os.path.join(save_image_path, file_name)
    with open(full_image_path, 'wb') as file:
        file.write(img_content)

    classifications = post.get("classifications")
    if not classifications:
        return json_response(status=400, data={'status_code': 400, 'reason': f'Incorrect request'})
    new_object = await Object.create(image_path=full_image_path, classification_set=list(map(int, classifications.split(','))))

    return json_response(status=201, data={
        'id': new_object.id
    })


async def get_objects(request: Request) -> Response:

    objects = await Object.all().values(
        id="id",
    )

    return Response(text=json.dumps(objects, default=str), content_type='application/json')
