import os
import sys
import yaml

from image_annotation_service.lib.log import log


class Config:
    def __init__(self, path: str = None) -> None:
        log.debug("class Config: __init__()")

        if os.path.isfile(path):
            with open(path) as f:
                self.__configYaml = yaml.load(f, Loader=yaml.SafeLoader)
        else:
            log.error('ERROR: file "%s" not found' % path)
            sys.exit()

    def get_conf(self):
        return self.__configYaml


def get_config() -> dict:
    obj_conf = Config(os.path.join(os.getcwd(), 'configs/config.yaml'))
    return obj_conf.get_conf()


config = get_config()

DATABASE_PATH = f'postgres://{config["postgres"]["user"]}' \
                f':{config["postgres"]["password"]}@{config["postgres"]["host"]}' \
                f':{config["postgres"]["port"]}/{config["postgres"]["database"]}'

TORTOISE_ORM_CONFIG = {
    "connections": {"default": DATABASE_PATH},
    "apps": {
        "models": {
            "models": [
                "image_annotation_service.models.models",
                "aerich.models",
            ],
            "default_connection": "default",
        },
    },
}
