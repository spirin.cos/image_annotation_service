import os
import json
from datetime import datetime
import logging.config
from logging import Handler, Formatter, LogRecord


monitoring_error_list = []


class LogFormatter(Formatter):
    def __init__(self):
        super(LogFormatter, self).__init__()

    def format(self, record: LogRecord) -> tuple:
        data = (record.levelname,
                datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                record.name,
                record.module,
                record.lineno,
                record.msg)
        return data


class RequestsHandler(Handler):
    def emit(self, record: LogRecord) -> None:
        if record.levelname in ['CRITICAL', 'ERROR']:
            log_entry = self.format(record)
            monitoring_error_list.append(log_entry)


current_dir = os.getcwd()
with open(os.path.join(current_dir, 'configs/logger.json'), 'r') as conf:
    logging.config.dictConfig(json.load(conf))

debug_level = logging.DEBUG
info_level = logging.INFO

log = logging.getLogger()
handler = RequestsHandler()

formatter = LogFormatter()
handler.setFormatter(formatter)
log.addHandler(handler)
