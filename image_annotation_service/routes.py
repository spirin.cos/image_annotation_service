from aiohttp import web

from image_annotation_service.lib.handles.authorization import login, get_authorization_data, logout
from image_annotation_service.lib.handles.handles import *


def setup_routes(app):
    app.add_routes([
        web.get('/', index, name='index'),

        web.post('/auth/login/', login, name='login'),
        web.get('/api/user/authorization/', get_authorization_data, name='authorization'),
        web.post('/auth/logout/', logout, name='logout'),


        web.get('/api/object/', get_objects, name='get_objects'),
        web.post('/api/object/', create_object, name='create_object'),

        web.post('/api/classification/', create_classification, name='create_classification'),
        web.get('/api/classification/', get_classification, name='get_classification'),
    ])
