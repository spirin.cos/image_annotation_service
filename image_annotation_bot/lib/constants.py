class EnterTypes:
    ENTER = 'Войти🔐'
    REGISTRATION = 'Регистрация📝'

    all = [ENTER, REGISTRATION]

    def __contains__(self, item):
        if item not in self.all:
            return False
        return True


class MainMenu:
    EXIT = 'Выйти'
    MY_TASK = 'Мои задания'

    all = [EXIT, MY_TASK]

    def __contains__(self, item):
        if item not in self.all:
            return False
        return True


class TaskMenu:
    EXIT = 'Закончить🚪'

    all = [EXIT]

    def __contains__(self, item):
        if item not in self.all:
            return False
        return True


ENTER_TYPES = EnterTypes()
MAIN_MENU_OPTIONS = MainMenu()
TASK_MENU_OPTIONS = TaskMenu()
