

import uuid

from aiogram import Bot, Dispatcher, executor, types, md
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import ParseMode
from tortoise import Tortoise

from image_annotation_bot.lib.constants import ENTER_TYPES, MAIN_MENU_OPTIONS, TASK_MENU_OPTIONS
from image_annotation_service.lib.configs import config, TORTOISE_ORM_CONFIG
from image_annotation_service.models import User, Object, Classification, ObjectClassification

API_TOKEN = config['telegram_bot_token']

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)

storage = MemoryStorage()

dp = Dispatcher(bot, storage=storage)

not_auth_keyboard = types.ReplyKeyboardMarkup(row_width=2)
not_auth_keyboard.add((types.KeyboardButton(ENTER_TYPES.REGISTRATION, )))
not_auth_keyboard.add((types.KeyboardButton(ENTER_TYPES.ENTER)))

auth_keyboard = types.ReplyKeyboardMarkup(row_width=2)
auth_keyboard.add((types.KeyboardButton('Мои задания', )))
auth_keyboard.add((types.KeyboardButton('Выйти', )))


class Enter(StatesGroup):
    type = State()


class AuthForm(StatesGroup):
    login = State()
    password = State()


class MainMenu(StatesGroup):
    task = State()
    menu = State()


@dp.message_handler(commands='start')
async def start_cmd_handler(message: types.Message):
    await Enter.type.set()
    await message.reply(md.text(
        md.text('Привет я бот для разметки изображений.'),
        md.text('Авторизуйтесь или зарегистрируйтесь.'),
        sep='\n',
    ),
        reply_markup=not_auth_keyboard
    )


@dp.message_handler(lambda message: message.text not in ENTER_TYPES, state=Enter.type)
async def process_enter_type_invalid(message: types.Message):
    return await message.reply("Для продолжения Вам необходимо авторизоваться или зарегистрироваться.\n"
                               "Выберите подходящий вариант на клавиатуре ниже", reply_markup=not_auth_keyboard)


@dp.message_handler(state=Enter.type)
async def process_enter(message: types.Message, state: FSMContext):
    async with state.proxy() as data:

        markup = types.ReplyKeyboardRemove()

        if message.text == ENTER_TYPES.ENTER:
            await AuthForm.login.set()

            await bot.send_message(
                message.chat.id,
                md.text('Введите логин'),
                reply_markup=markup,
                parse_mode=ParseMode.MARKDOWN,
            )
        if message.text == ENTER_TYPES.REGISTRATION:
            uid = f'{message.from_user.username}-{str(uuid.uuid4())[0:6]}'
            await User.filter(telegram_id=message.from_user.id).update(telegram_id=None)
            user = await User.create(
                uid=uid,
                telegram_id=message.from_user.id,
                password=str(uuid.uuid4())[0:8]
            )
            await MainMenu.menu.set()

            await bot.send_message(
                message.chat.id,
                md.text(f'Вы успешно зарегистрировались\nВаш логин: {user.uid}\nВаш пароль: {user.password}'),
                reply_markup=auth_keyboard,
                parse_mode=ParseMode.MARKDOWN,
            )


@dp.message_handler(state=AuthForm.login)
async def process_login(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['uid'] = message.text

        await AuthForm.password.set()

        await bot.send_message(
            message.chat.id,
            md.text('Введите пароль'),
            parse_mode=ParseMode.MARKDOWN,
        )


@dp.message_handler(state=AuthForm.password)
async def process_login(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['password'] = message.text

        await AuthForm.password.set()

        user = await User.filter(uid=data['uid'], password=data['password']).first()
        if user:
            await MainMenu.menu.set()
            data['user'] = user
            await User.filter(telegram_id=message.from_user.id).update(telegram_id=None)
            await User.filter(id=user.id).update(telegram_id=message.from_user.id)
            await bot.send_message(
                message.chat.id,
                md.text('Вы авторизованы'),
                parse_mode=ParseMode.MARKDOWN,
                reply_markup=auth_keyboard
            )
        else:
            await state.finish()
            await Enter.type.set()
            data['user'] = user
            await bot.send_message(
                message.chat.id,
                md.text('Неправильный логин или пароль,\nпопробуете еще раз?'),
                parse_mode=ParseMode.MARKDOWN,
                reply_markup=not_auth_keyboard
            )


@dp.message_handler(lambda message: message.text not in MAIN_MENU_OPTIONS, state=MainMenu.menu)
async def process_menu_invalid(message: types.Message):
    return await message.reply("Выберите доступное действие на клавиатуре", reply_markup=auth_keyboard)


@dp.message_handler(state=MainMenu.menu)
async def process_main_menu(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if message.text == MAIN_MENU_OPTIONS.EXIT:
            await state.finish()
            await Enter.type.set()
            await bot.send_message(
                message.chat.id,
                md.text('Вы успешно вышли'),
                parse_mode=ParseMode.MARKDOWN,
                reply_markup=not_auth_keyboard
            )
        elif message.text == MAIN_MENU_OPTIONS.MY_TASK:
            await state.finish()
            await MainMenu.task.set()

            user: User = await User.filter(telegram_id=message.from_user.id).first()

            tasks = await user.get_unmarked_objects()
            confirm_keyboard = types.ReplyKeyboardMarkup(row_width=2)
            confirm_keyboard.add((types.KeyboardButton('получить задание', )))
            await bot.send_message(
                message.chat.id,
                md.text(f'Доступно: {len(tasks)}'),

                parse_mode=ParseMode.MARKDOWN,
                reply_markup=confirm_keyboard
            )


async def get_task(user: User, data: dict, message: types.Message):
    task_object: Object = await user.get_unmarked_object()

    if not task_object:
        await MainMenu.menu.set()
        await bot.send_message(
            message.chat.id,
            md.text('🎉🎉🎉Спасибо, все доступные на данный момент задания выполнены\nВозвращайтесь позже'),
            parse_mode=ParseMode.MARKDOWN,
            reply_markup=auth_keyboard
        )
        return

    data['task_object'] = task_object
    media = types.MediaGroup()
    media.attach_photo(types.InputFile(task_object.image_path), 'Что на изображении?')
    await message.reply_media_group(media=media, reply=False)
    classifications = await Classification.filter(id__in=task_object.classification_set).all()

    dict_classifications = {}

    task_keyboard = types.ReplyKeyboardMarkup(row_width=2)

    for classification in classifications:
        dict_classifications[classification.name] = classification
        task_keyboard.add((types.KeyboardButton(classification.name, )))
    data['classifications'] = dict_classifications
    task_keyboard.add((types.KeyboardButton(TASK_MENU_OPTIONS.EXIT, )))
    await bot.send_message(
        message.chat.id,
        md.text('.'),
        parse_mode=ParseMode.MARKDOWN,
        reply_markup=task_keyboard

    )


@dp.message_handler(state=MainMenu.task)
async def process_main_menu(message: types.Message, state: FSMContext):
    async with state.proxy() as data:

        classifications = data.get('classifications')

        user: User = data.get('user')

        if not user:
            user: User = await User.filter(telegram_id=message.from_user.id).first()

        if message.text == TASK_MENU_OPTIONS.EXIT:
            await MainMenu.menu.set()
            await bot.send_message(
                message.chat.id,
                md.text('Выполнение заданий завершено'),
                parse_mode=ParseMode.MARKDOWN,
                reply_markup=auth_keyboard
            )
        elif classifications:

            if message.text in classifications.keys():
                task_object: Object = data['task_object']
                classification: Classification = classifications[message.text]
                await ObjectClassification.create(object_id=task_object.id, classification_id=classification.id,
                                                  user_id=user.id)
                await get_task(user, data, message)
        else:
            await get_task(user, data, message)


async def init_db(_dp):
    await Tortoise.init(config=TORTOISE_ORM_CONFIG)
    await Tortoise.generate_schemas()


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=init_db)
